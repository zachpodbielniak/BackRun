# BackRun

Backup Tool To Backup Files To S3

## License

![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)

```
 ____             _    ____              
| __ )  __ _  ___| | _|  _ \ _   _ _ __  
|  _ \ / _` |/ __| |/ / |_) | | | | '_ \ 
| |_) | (_| | (__|   <|  _ <| |_| | | | |
|____/ \__,_|\___|_|\_\_| \_\__,_|_| |_|
                                         


Backup Tool To Backup Files To S3
Copyright (C) 2019 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

BackRun is licensed under the AGPLv3 license. Don't like it? Go else where.


## Donate
Like BackRun? You use it yourself, or for your infrastructure? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59

## Docker
There is a pre-built Docker image at zachpodbielniak/backrun. Or you can utilize the provided Dockerfile to build it yourself. Below is how to run this:

```
docker run 						\
	--name backrun					\
	-v /path/to/your/public/key.asc:/gpg.asc	\
	-v /dir/you/want/backed/up:/backup 		\
	-v /home/user/.aws:/root/.aws			\
	-e BUCKET="your-s3-bucket"			\
	-e BUCKETDIR="path/in/bucket"			\
	-e GPGEMAIL="your-email@example.com"		\
	zachpodbielniak/backrun
```