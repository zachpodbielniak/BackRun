#!/usr/bin/bash

#  ____             _    ____              
# | __ )  __ _  ___| | _|  _ \ _   _ _ __  
# |  _ \ / _` |/ __| |/ / |_) | | | | '_ \ 
# | |_) | (_| | (__|   <|  _ <| |_| | | | |
# |____/ \__,_|\___|_|\_\_| \_\__,_|_| |_|
#                                          
# 
# 
# Backup Tool To Backup Files To S3
# Copyright (C) 2019 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


DIR="/backup"

if [ "$BUCKET" = "" ]
then
	echo "\$BUCKET Is NULL"
	exit 1
fi

if [ "$BUCKETDIR" = "" ]
then
	echo "\$BUCKETDIR Is NULL"
	exit 1
fi 

if [ "$GPGEMAIL" = "" ]
then 
	echo "\$GPGEMAIL Is NULL"
	exit 1
fi 


S3PATH="s3://$BUCKET/$BUCKETDIR"
echo "S3PATH=$S3PATH"

OLD_DIR=$(pwd)
cd "$DIR"

for DIR_ITERATOR in "./"*
do 
	FILE=${DIR_ITERATOR:2}
	echo "Backing Up \"$DIR/$FILE\" => \"$S3PATH/$FILE.tar.gz.gpg\""
	tar -c "./$FILE" | pigz -9 | gpg -e --trust-model always -r "$GPGEMAIL" | aws s3 cp - "$S3PATH/$FILE.tar.gz.gpg"
done

if [ "$GOTIFYURL" -ne "" ]
then 
	curl -4 -X POST "$GOTIFYURL" -F "title=BackRun Job Completed" -F "message=Data backed up to $S3PATH." > /dev/null 2>&1
fi

cd "$OLD_DIR"