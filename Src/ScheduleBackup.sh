#!/usr/bin/bash

#  ____             _    ____              
# | __ )  __ _  ___| | _|  _ \ _   _ _ __  
# |  _ \ / _` |/ __| |/ / |_) | | | | '_ \ 
# | |_) | (_| | (__|   <|  _ <| |_| | | | |
# |____/ \__,_|\___|_|\_\_| \_\__,_|_| |_|
#                                          
# 
# 
# Backup Tool To Backup Files To S3
# Copyright (C) 2019 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


GPG_KEY="/path/to/your/public/key"
GPG_EML="your@email.com"

echo "Path To Backup?"
read B_PATH
echo "S3 Bucket?"
read B_BUCKET
echo "Bucket Directory?"
read B_DIR
B_USER=$(whoami)
B_NAME="backrun-$B_BUCKET"

docker run 						\
	-d 						\
	--name backrun					\
	-v $GPG_KEY:/gpg.asc				\
	-v $B_PATH:/backup 				\
	-v /home/$B_USER/.aws:/root/.aws		\
	-e BUCKET="$B_BUCKET"				\
	-e BUCKETDIR="$B_DIR"				\
	-e GPGEMAIL="$GPG_EML"				\
	zachpodbielniak/backrun
